import { writable, derived } from 'svelte/store';

function setTheme(){
	const { subscribe, update } = writable('light');

	return {
		subscribe,
		dark: ()=> update( 
			n => 'dark'	
		),
		light: ()=> update( 
			n => 'light' 
		)
	};
};

export const Theme = setTheme();

export const Colors = derived(
	Theme,
	$Theme => 
		$Theme === 'light' ? { 
			main:'#7892ff', 
			secondary:'#eee5e9', 
			shadow:'#2b303a', 
			alternative:'#b8b8d1', 
			focal:'#ee6352',
		}:{ 
			main:'#2f004f', 
			secondary:'#d8ddef', 
			shadow:'#0d1b1e', 
			alternative:'#b8b8d1', 
			focal:"#a4508b", 
		}
);
//#5f0a87
