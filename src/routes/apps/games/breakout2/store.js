import { writable, derived } from 'svelte/store';

export const current = writable({
	level: 1,
	lives: 3,
	score: 0,
});

export const game = derived( current,({$level})=> {
	const width = 400;
	const height = 200;
	const rows = 4;
	const cols = 10;

	return{
		w: width,
		h: height,
		x: 0,
		y: 0,
		bricks: { rows, cols, stX:width/(cols*2), stY:height/(rows*2.5), h:height/(rows*5), w:width/(cols*2), p:5 },
	}
});

const drawBricks = ({ rows, cols, stX, stY, h, w, p })=> Array.from(
	{ length: rows }, 
	( _, row ) => Array.from(
		{ length: cols }, 
		( _, col ) => {
			let x = col*( w+stX)+p;
			let y = row*( h+stY)+p;
			return { id: x+","+y, x, y, h, w  };
		}
	)
);

export const bricks = derived(
	game,
	$game => drawBricks( $game.bricks ).flat()
);


function drawBall(){
	const { subscribe, set, update } = writable({ x:200, y:175, r:8, sx:1, sy:1 });
	return {
		subscribe,
		start: ()=> update(({ x, y, r, sx, sy })=>({ x: x+sx, y: y-sy, r, sx, sy})),

		flipX: ()=> update(({ x, y, r, sx, sy })=>({ x, y, r, sx: -sx, sy })),

		flipY: ()=> update(({ x, y, r, sx, sy })=>({ x, y, r, sx, sy: -sy })),

		reset: ()=> set({ x:100, y:80, r:5, sx:1, sy:1 })
	};
};

export const ball = drawBall();

function drawPaddle(){
	const { subscribe, set, update } = writable({ x:175, y:185, h:10, w:50, s:7 });
	return {
		subscribe,
		moveL: ()=> update(({ x, y, w, h, s })=>({ x: x-s, y, w, h, s })),

		moveR: ()=> update(({ x, y, w, h, s })=>({ x: x+s, y, w, h, s })),

		reset: ()=> set({ x:85, y:85, h:5, w:30, s: 7 })
	};
};

export const paddle = drawPaddle();
