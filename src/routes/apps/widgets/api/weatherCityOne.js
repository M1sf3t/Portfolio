require('dotenv/config')

const url ="https://api.openweathermap.org/data/2.5/forecast"
const key = process.env.WEATHER 

export async function get( req, res, next ){
	const q = req.query;
	const query = `${ url }?q=${ q.city }&units=imperial&appid=${ key }`

	const cityWeather = await require('https').get( query, resp =>{
		resp.pipe(res);
	})

	cityWeather.on('error', error => {
		console.error( error )
	})

	if( cityWeather === null ){
		next();	
	} 
};
