import { writable, readable, derived } from 'svelte/store'

export const locations = readable([
	{ city: 'Jackson', state: 'MS' },
	{ city: 'Seattle', state: 'WA' },
	{ city: 'Boston', state: 'MA' }
]);

export const weatherData = writable([]);

export const selectedCity=writable(0);

export const selectedDay=writable(0);

export const cityData = derived( 
	[ weatherData, selectedCity ],([ $weatherData, $selectedCity ])=>{ 
		return Array(5).fill([]).map(( day, min )=>{
			return $weatherData[ $selectedCity ].list.filter(( _, i )=> i >= min*8 && i < ( min+1 )*8 );
		});
	}
);

export const weeklyData = derived( cityData, $cityData => {
	return $cityData.map( day => {
		let temps = day.map( t => t.main.temp );
		let dates = day.map( d => d.dt_txt.split(' '));

		return { 	
			date: dates,
			times: dates.map( time => time[1] ),
			temps: temps,
			low: Math.min( ...temps ),
			high: Math.max( ...temps ),
		};
	});
});

	
export const graphData = derived(
	[ weeklyData, selectedDay ], ([ $weeklyData, selectedDay ])=>{ 

		const { date, times, temps } = $weeklyData[ selectedDay ]

		return {
			scaleX: 10,
			scaleY: 10,
			intX: 1,
			intY: 10,
			dataX: times.map( time => time[1].slice(0,2)).map( val =>  val>12? `${val-12}pm`: val===12? `{val}pm` 
				: val>9? `${val}am`: val==0? "12am":`${val.slice(0,2)}am`),
			dataY: temps,
		}
	}
);

